#!/bin/bash

# This test represents most likely workflow for using SABNA.

N=8
IN=data/asia.csv
OUT=asia
PRIOR=data/asia.sabna.priors

tools/csv-prepare.py $IN $OUT

if [ -f "$PRIOR" ]; then
  bin/sabna-exsl-mpsbuild --csv-file $OUT/$OUT.sabna.csv --mps-file $OUT/$OUT.sabna.mps --priors-file $PRIOR
else
  bin/sabna-exsl-mpsbuild --csv-file $OUT/$OUT.sabna.csv --mps-file $OUT/$OUT.sabna.mps
fi

bin/sabna-exsl-ssearch -n $N --mps-file $OUT/$OUT.sabna.mps --ord-file $OUT/$OUT.sabna.ord --scc-limit 4
bin/sabna-order2net --csv-file $OUT/$OUT.sabna.csv --mps-file $OUT/$OUT.sabna.mps --ord-file $OUT/$OUT.sabna.ord --net-name $OUT/$OUT.sabna --format sif
bin/sabna-pl-mle --csv-file $OUT/$OUT.sabna.csv --sif-file $OUT/$OUT.sabna.sif --bif-file $OUT/$OUT.sabna.bif

tools/bif-format.py --map-variables $OUT/$OUT.sabna.variables --map-states $OUT/$OUT.sabna.states $OUT/$OUT.sabna.bif $OUT.bif $OUT
tools/sif-format.py --map-variables $OUT/$OUT.sabna.variables $OUT/$OUT.sabna.sif $OUT.sif
