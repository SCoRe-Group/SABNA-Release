#!/usr/bin/env python3

__author__ = "Jaroslaw Zola"
__copyright__ = "Copyright (c) 2021 SCoRe Group http://www.score-group.org/"
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Jaroslaw Zola"
__email__ = "jaroslaw.zola@hush.com"
__status__ = "Development"

import argparse
import os
import sys


def extant_file(fname):
    if not os.path.isfile(fname): raise argparse.ArgumentTypeError("file {0} not found".format(fname))
    return fname


if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("input", metavar="", help = "input SABNA priors file", type = extant_file)
    parser.add_argument("names", metavar="", help = "SABNA variables file", type = extant_file)
    parser.add_argument("output", metavar="", help = "output priors file")

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(-1)

    args = parser.parse_args()

    infile = args.input
    names = args.names
    outfile = args.output

    N = {}

    N["-"] = "-"
    N["!"] = "!"
    N["*"] = "*"

    with open(names, "r") as fnames:
        for line in fnames:
            (key, val) = line.split()
            N[key] = val

    with open(infile, "r") as finput:
        with open(outfile, "w") as foutput:
            for line in finput:
                L = [ N[x] for x in line.split()]
                for x in L: foutput.write(x + ' ')
                foutput.write("\n")
