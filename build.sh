#!/bin/bash

if [ -n "$1" ]; then
  DIR="$1"
else
  DIR=`pwd`
fi

# update revision id (do not edit)
REV=`git rev-parse HEAD`
echo "#define SABNA_REVISION \"$REV\"" > src/revision.hpp

# prepare directories structure (do not edit)
mkdir -p build/
rm -rf build/*
cd build/


# Add -DCMAKE_BUILD_TYPE=Debug to debug
cmake ../ -DCMAKE_INSTALL_PREFIX=$DIR

#make -j4 install VERBOSE=1
make -j4 install
