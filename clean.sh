#!/bin/bash

cd SABNAtk/
./clean.sh
cd ..

cd containers/singularity/
./clean.sh
cd ../..

rm -rf tools/__pycache__
rm -rf bin/
rm -rf build/
rm -rf examples/
rm -rf asia*

echo "#define SABNA_REVISION \"\"" > src/revision.hpp
