/***
 *  $Id$
 **
 *  File: sabna-exsl-mpsbuild.cpp
 *  Created: Aug 22, 2017
 *
 *  Author: Subhadeep Karan <skaran@buffalo.edu>
 *          Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2017-2019 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#include <algorithm>
#include <chrono>
#include <iostream>
#include <limits>
#include <vector>

#include <cxxopts/cxxopts.hpp>

#include <sabnatk.hpp>

#include "AICEngine.hpp"
#include "BDeuEngine.hpp"
#include "MDLEngine.hpp"
#include "MPSBuild.hpp"
#include "Priors.hpp"
#include "config.hpp"
#include "csv.hpp"
#include "log.hpp"


using data_type = uint8_t;


struct Functors {
    AIC aic;
    MDL mdl;
    BDeu bdeu;
}; // struct Functors


template <int N>
std::pair<bool, std::string> build_and_write(int n, int m, std::vector<data_type>& D, const std::string& mps_name, const std::string& priors_name, const std::string& scoring_f, Functors& func, const std::string& cqe_type, int pa_size, int min_dfs) {
    Log.debug() << "processing with N = " << N << " " << sizeof(bit_util_base_type) << "B words" << std::endl;

    Priors<N> priors;

    if (!priors_name.empty()) {
        // Log.warn() << "support for priors is experimental, caution advised!" << std::endl;
        Log.debug() << "reading priors" << std::endl;
        auto res = priors.read(priors_name, n);
        if (!res.first) return res;

        if ((scoring_f == "aic") || (scoring_f == "bdeu")) {
            Log.warn() << scoring_f << " does not support priors yet" << std::endl;
        }
    }

    MPSBuild<N> sl_mps;

    auto start = std::chrono::system_clock::now();

    if (cqe_type == "rad") {
        Log.debug() << "building RadCounter engine" << std::endl;
        sabnatk::RadCounter<N> rad = sabnatk::create_counter<N, sabnatk::Rad, data_type>(n, m, std::begin(D));
        auto diff = std::chrono::system_clock::now() - start;
        Log.debug() << "engine ready in " << std::chrono::duration_cast<std::chrono::microseconds>(diff).count() << "ms" << std::endl;

        if (scoring_f == "aic") sl_mps.run(AICEngine<N, sabnatk::RadCounter<N, data_type>>(rad, func.aic, pa_size), min_dfs);
        else if (scoring_f == "mdl") sl_mps.run(MDLEngine<N, sabnatk::RadCounter<N, data_type>>(rad, func.mdl, priors, pa_size), min_dfs);
        else sl_mps.run(BDeuEngine<N, sabnatk::RadCounter<N, data_type>>(rad, func.bdeu, pa_size), min_dfs);
    } else {
        Log.debug() << "building BVCounter engine" << std::endl;
        sabnatk::BVCounter<N> bvc = sabnatk::create_counter<N, sabnatk::BV>(n, m, std::begin(D));
        auto diff = std::chrono::system_clock::now() - start;
        Log.debug() << "engine ready in " << std::chrono::duration_cast<std::chrono::microseconds>(diff).count() << "ms" << std::endl;

        if (scoring_f == "aic") sl_mps.run(AICEngine<N, sabnatk::BVCounter<N>>(bvc, func.aic, pa_size), min_dfs);
        else if (scoring_f == "mdl") sl_mps.run(MDLEngine<N, sabnatk::BVCounter<N>>(bvc, func.mdl, priors, pa_size), min_dfs);
        else sl_mps.run(BDeuEngine<N, sabnatk::BVCounter<N>>(bvc, func.bdeu, pa_size), min_dfs);
    }

    Log.info() << "writing MPS with " << sl_mps.size() << " parent sets" << std::endl;
    auto res = sl_mps.write(mps_name);

    return res;
} // build_and_write


int main (int argc, const char* argv[]) {
    InitLog();
    score_message("sabna-exsl-mpsbuild");

    std::string csv_name;
    std::string mps_name;
    std::string priors_name;
    std::string scoring_f;
    double alpha;
    std::string cqe_type;
    int pa_size;
    int min_dfs;

    try {
        cxxopts::Options options(argv[0]);

        options.add_options()
            ("csv-file", "input data file, column-wise without header, white space separated", cxxopts::value<std::string>(csv_name))
            ("mps-file", "output mps file", cxxopts::value<std::string>(mps_name))
            ("priors-file", "hard priors file", cxxopts::value<std::string>(priors_name)->default_value(""))
            ("cqe", "counting query engine [auto|rad|bvc]", cxxopts::value<std::string>(cqe_type)->default_value("auto"))
            ("s,score", "scoring function [aic|mdl|bdeu]", cxxopts::value<std::string>(scoring_f)->default_value("mdl"))
            ("a,alpha", "BDeu hyper-parameter", cxxopts::value<double>(alpha)->default_value("1.0"))
            ("l,pa-size", "parent set size limit", cxxopts::value<int>(pa_size)->default_value("-1"))
            ("d,dfs-level", "level to switch to DFS", cxxopts::value<int>(min_dfs)->default_value("-1"))
            ("help", "print this help")
            ;

        auto opt_res = options.parse(argc, argv);

        if (!(opt_res.count("csv-file") && opt_res.count("mps-file")) || opt_res.count("help")) {
            std::cout << options.help() << std::endl;
            return 0;
        }

        if ((cqe_type != "auto") && (cqe_type != "rad") && (cqe_type != "bvc")) throw cxxopts::OptionException("Incorrect " + cxxopts::LQUOTE + "cqe" + cxxopts::RQUOTE + " option");
        if ((scoring_f != "aic") && (scoring_f != "mdl") && (scoring_f != "bdeu")) throw cxxopts::OptionException("Incorrect " + cxxopts::LQUOTE + "score" + cxxopts::RQUOTE + " option");
        if (alpha < 0.0) throw cxxopts::OptionException("Incorrect " + cxxopts::LQUOTE + "alpha" + cxxopts::RQUOTE + " option");

        if (!opt_res.unmatched().empty()) throw cxxopts::OptionException("Unrecognized options");
    } catch (const cxxopts::OptionException& e) {
        Log.error() << e.what() << std::endl;
        return -1;
    }

    std::vector<data_type> D;
    auto [b, n, m] = read_csv(csv_name, D);

    if (b == false) {
        Log.error() << "could not read input data" << std::endl;
        return -1;
    }

    Log.info() << "input: " << csv_name << std::endl;
    Log.info() << "variables: " << n << std::endl;
    Log.info() << "instances: " << m << std::endl;
    Log.info() << "function: " << scoring_f << std::endl;
    Log.info() << "limit: " << pa_size << std::endl;
    Log.info() << "priors: " << priors_name << std::endl;
    Log.info() << "output: " << mps_name << std::endl;

    // we use m=800 as a threshold between bvc and rad
    // for small m bvc is much better
    if (cqe_type == "auto") {
        if (m < 801) cqe_type = "bvc"; else cqe_type = "rad";
        Log.info() << "engine: " << cqe_type << std::endl;
    }

    Log.info() << "searching parent sets" << std::endl;
    auto start = std::chrono::system_clock::now();

    // initialize functors based on provided and default hyper-parameters
    Functors func;

    func.aic = AIC();
    func.mdl = MDL(m);
    func.bdeu = BDeu(alpha);

    std::pair<bool, std::string> res;

    if (n <= set_max_item<1>())      res = build_and_write<1>(n, m, D, mps_name, priors_name, scoring_f, func, cqe_type, pa_size, min_dfs);
    else if (n <= set_max_item<2>()) res = build_and_write<2>(n, m, D, mps_name, priors_name, scoring_f, func, cqe_type, pa_size, min_dfs);
    else if (n <= set_max_item<3>()) res = build_and_write<3>(n, m, D, mps_name, priors_name, scoring_f, func, cqe_type, pa_size, min_dfs);
    else if (n <= set_max_item<4>()) res = build_and_write<4>(n, m, D, mps_name, priors_name, scoring_f, func, cqe_type, pa_size, min_dfs);
    else {
        Log.error() << "unable to handle more than " << set_max_item<4>() << " variables!";
        return -1;
    }

    if (!res.first) {
        Log.error() << res.second << std::endl;
        return -1;
    }

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = end - start;
    Log.info() << "searching done in " << jaz::log::second_to_time(diff.count()) << std::endl;

    return 0;
} // main
