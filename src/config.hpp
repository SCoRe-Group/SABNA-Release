#ifndef CONFIG_HPP
#define CONFIG_HPP

#define SABNA_VERSION "3.010"
#include "revision.hpp"

inline void score_message(const std::string& name = "") {
    std::cout << "SABNA: Scalable Accelerated Bayesian Network Analytics, version " << SABNA_VERSION << std::endl;
    if (!std::string(SABNA_REVISION).empty()) std::cout << "Revision: " << SABNA_REVISION << std::endl;
    std::cout << "Copyright (c) 2016-2022 SCoRe Group http://www.score-group.org/" << std::endl;
    std::cout << "https://gitlab.com/SCoRe-Group/SABNA-Release" << std::endl;
    std::cout << name << std::endl;
} // score_message

#endif // CONFIG_HPP
