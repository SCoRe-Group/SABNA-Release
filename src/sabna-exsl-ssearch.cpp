/***
 *  $Id$
 **
 *  File: sabna-exsl-ssearch.cpp
 *  Created: Apr 10, 2019
 *
 *  Authors: Subhadeep Karan <skaran@buffalo.edu>
 *           Jaroslaw Zola <jaroslaw.zola@hush.com>
 *           Zainul Abideen Sayed <zsayed@buffalo.edu>
 *  Copyright (c) 2017-2022 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdint>
#include <iostream>
#include <vector>

#include <tbb/global_control.h>
#include <tbb/parallel_for.h>
#include <tbb/partitioner.h>
#include <tbb/spin_mutex.h>

#include <cxxopts/cxxopts.hpp>
#include <jaz/math.hpp>
#include <parallel_hashmap/phmap.h>

#include <bit_util.hpp>

#include "MPSList.hpp"
#include "config.hpp"
#include "graph_util.hpp"
#include "log.hpp"


// empirically this is the best size for most problems
const int BUCKETS = 65536;


struct SCC_config {
    int scc_limit;
    float scc_density;
}; // struct SCC_config

template <int N> struct search_data {
    SCC_config scc_conf;
    MPSList<N> mps_list;
    std::vector<std::pair<uint_type<N>, double>> opt;
}; // struct search_data


template <int N> struct bfs_node {
    using set_type = uint_type<N>;
    set_type id = set_empty<set_type>();

    static const int PATH_SIZE = set_max_item<N>() + 1;

    void merge(const bfs_node& t) {
        if (score > t.score) {
            score = t.score;
            std::copy(t.path, t.path + PATH_SIZE, path);
            suboptimal_extension = t.suboptimal_extension;
        }
    } // merge

    double score;
    uint8_t path[PATH_SIZE];
    uint_type<N> suboptimal_extension;
}; // struct bfs_node

template <int N>
inline bool operator==(const bfs_node<N>& t1, const bfs_node<N>& t2) {
    return (t1.id == t2.id);
} // operator==

namespace std {

  template <int N> struct hash<bfs_node<N>> {
      std::size_t operator()(const bfs_node<N>& x) const noexcept {
          return h(x.id);
      } // operator()

      uint_hash h;
  }; // struct hash

} // namespace std

template <int N> struct hyper_partitioner {
    explicit hyper_partitioner() { }

    unsigned long long operator()(const uint_type<N>& t, int n) const {
        auto val = t;
        val = shift_right(val, (n >> 1));
        return h(val);
    } // operator()

    uint_hash h;
}; // struct hyper_partitioner


template <int N> using storage_type = phmap::flat_hash_set<bfs_node<N>>;
template <int N> using tasks_table = std::vector<storage_type<N>>;
template <int N> using partitioner = hyper_partitioner<N>;


using mutex_type = tbb::spin_mutex;
std::vector<std::vector<mutex_type>> gmtx_;


// returns size and location of first non-zero bucket
template <typename Container>
inline std::pair<std::size_t, std::size_t> container_size(const Container& C) {
    std::size_t count = 0;
    std::size_t first_pos = 0;
    for (auto& x : C) {
        count += x.size();
        if (!count) ++first_pos;
    }
    return  {count, first_pos};
} // container_size


template <int N>
void vanilla_bfs(const search_data<N>& sdat, const uint_type<N>& to_add, uint_type<N>& source_id, bfs_node<N>& source_node) {
    using set_type = uint_type<N>;
    int n = sdat.mps_list.n();

    int source_id_size = set_size(source_id);
    int to_add_size = set_size(to_add);
    int target_id_size = source_id_size + to_add_size;

    set_type goal_id = source_id;

    for (int x = 0; x < set_max_size<set_type>(); ++x)
        if(in_set(to_add, x)) goal_id = set_add(goal_id, x);

    tasks_table<N> vec_ht(target_id_size + 1);
    vec_ht[source_id_size - source_id_size].insert(source_node);

    for (int l = 0; l <= target_id_size; ++l) {
        if (vec_ht[l].size() == 1 && vec_ht[l].begin()->id == goal_id) {
            source_id = goal_id;
            source_node = *vec_ht[l].begin();
            return;
        }

        for (auto& pa_node : vec_ht[l]) {
            for (int xi = 0; xi < n; ++xi) {
                if (in_set(to_add, xi) && !in_set(pa_node.id, xi)) {
                    bfs_node<N> ch_node = pa_node;
                    set_type ch_node_id = set_add(pa_node.id, xi);
                    ch_node.score += sdat.mps_list.find(xi, pa_node.id).s;

                    int ch_node_id_size = set_size(ch_node_id);
                    ch_node.path[ch_node_id_size - 1] = xi;

                    optimal_path_extension(sdat, to_add, ch_node_id, ch_node, ch_node_id_size);
                    ch_node.id  = ch_node_id;

                    int idx = ch_node_id_size - source_id_size;
                    auto it = vec_ht[idx].find(ch_node);

                    if (it != vec_ht[idx].end()) {
                        if (ch_node.score < it->score) {
                            auto x = vec_ht[idx].extract(it);
                            x.value().merge(ch_node);
                            vec_ht[idx].insert(std::move(x));
                        }
                    } else {
                        vec_ht[idx].insert(std::move(ch_node));
                    }
                }
            } // for xi
        } // for pa_node
    } // for l
} // vanilla_bfs

template <int N>
bool scc_optimization(const search_data<N>& sdat, const uint_type<N>& target_id, uint_type<N>& nd_id, bfs_node<N>& nd) {
    if (sdat.scc_conf.scc_limit < 3) return false;

    using set_type = uint_type<N>;

    set_type to_add = set_diff(target_id, nd_id);
    std::vector<set_type> adj = sdat.mps_list.subset_adjacency_matrix(nd_id, to_add);

    // get DAG density
    int ne = 0;
    auto n = adj.size();

    for (auto& x : adj) ne += set_size(x);

    double dd = (1.0 * ne) / (n * n - n);

    // if dd is over this constant we have low probability of the success
    if (dd > sdat.scc_conf.scc_density) return false;

    std::vector<set_type> scc_list;

    scc_list.reserve(sdat.mps_list.n());
    build_kernel_dag(to_add, adj, scc_list);

    nd.suboptimal_extension = set_empty<set_type>();

    int scc_idx = 0;

    for (; scc_idx < scc_list.size() && set_size(scc_list[scc_idx]) <= sdat.scc_conf.scc_limit; ++scc_idx) {
        vanilla_bfs(sdat, scc_list[scc_idx], nd_id, nd);
    }

    ++scc_idx;

    for (; scc_idx < scc_list.size(); ++scc_idx) {
        nd.suboptimal_extension = nd.suboptimal_extension | scc_list[scc_idx];
    }

    return true;
} // scc_optimization

template <int N>
inline void optimal_path_extension(const search_data<N>& sdat, const uint_type<N>& to_add, uint_type<N>& id, bfs_node<N>& node, int& id_size) {
    int n = sdat.mps_list.n();
    bool extend = true;

    for (int i = 0; i < n && extend; ++i) {
        extend = false;
        for (int xi = 0; xi < n; ++xi) {
            if (!in_set(id, xi) && is_superset(id, sdat.opt[xi].first) && in_set(to_add, xi)) {
                extend = true;
                id = set_add(id, xi);
                node.score += sdat.opt[xi].second;
                node.path[id_size++] = xi;
            }
        } // for xi
    } // for i
} // optimal_path_extension


template <int N> void bfs_task(const search_data<N>& sdat,
                               const uint_type<N>& target_id,
                               const bfs_node<N>& pa_node,
                               int xi,
                               int l,
                               std::vector<tasks_table<N>>& v_nl) {
    using set_type = uint_type<N>;
    int n = sdat.mps_list.n();

    bfs_node<N> ch_node = pa_node;
    set_type ch_id = set_add(pa_node.id, xi);
    ch_node.score += sdat.mps_list.find(xi, pa_node.id).s;

    int ch_id_size = l;
    ch_node.path[ch_id_size++] = xi;

    // apply OPE
    optimal_path_extension(sdat, target_id, ch_id, ch_node, ch_id_size);
    ch_node.id  = ch_id;

    partitioner<N> pt;

    auto lyr = set_size(ch_id_size);
    auto bkt = pt(ch_node.id, n) % static_cast<std::size_t>(BUCKETS);

    {
        const std::lock_guard<mutex_type> lock(gmtx_[lyr][bkt]);
        auto it = v_nl[lyr][bkt].find(ch_node);

        if (it != v_nl[lyr][bkt].end()) {
            if (it->score > ch_node.score) {
                auto x = v_nl[lyr][bkt].extract(it);
                x.value().merge(ch_node);
                v_nl[lyr][bkt].insert(std::move(x));
            } else return;
        } // if
    }

    scc_optimization(sdat, target_id, ch_id, ch_node);
    ch_node.id  = ch_id;

    lyr = set_size(ch_node.id);
    bkt = pt(ch_node.id, n) % static_cast<std::size_t>(BUCKETS);

    {
        const std::lock_guard<mutex_type> lock(gmtx_[lyr][bkt]);
        auto it = v_nl[lyr][bkt].find(ch_node);

        if (it != v_nl[lyr][bkt].end()) {
            if (it->score > ch_node.score) {
                auto x = v_nl[lyr][bkt].extract(it);
                x.value().merge(ch_node);
                v_nl[lyr][bkt].insert(std::move(x));
            }
        }
        else v_nl[lyr][bkt].insert(std::move(ch_node));
    }

} // bfs_task


template <int N>
bfs_node<N> bfs_core(const search_data<N>& sdat,
                     const uint_type<N>& source_id,
                     const bfs_node<N>& source_node,
                     const uint_type<N>& target_id) {

    Log.info() << "starting search..." << std::endl;

    using set_type = uint_type<N>;
    int n = sdat.mps_list.n();

    int source_id_size = set_size(source_id);

    bfs_node<N> target_node;
    target_node.score = -1;

    std::vector<tasks_table<N>> v_nl(n + 1, tasks_table<N>(BUCKETS));
    partitioner<N> pt;

    auto bkt = pt(source_id, n) % static_cast<std::size_t>(BUCKETS);
    v_nl[source_id_size][bkt].insert(source_node);

    // process all layers
    long long int visited = 0;

    for (int l = source_id_size; l <= n; ++l) {
        auto [sz, loc] = container_size(v_nl[l]);

        if (sz == 0) continue;
        else if ((sz == 1) && (std::begin(v_nl[l][loc])->id == target_id)) {
            // yeah - we found a solution
            target_node = *std::begin(v_nl[l][loc]);
            break;
        }

        // we do Breadth Edge First expansion
        for (int xi = 0; xi < n; ++xi) {
            using range_type = typename tbb::blocked_range<std::size_t>;
            auto range = tbb::blocked_range<std::size_t>(0, BUCKETS, 12);

            static tbb::simple_partitioner sp;

            tbb::parallel_for(range, [&](range_type r) {

                for (std::size_t bkt = r.begin(); bkt < r.end(); ++bkt) {
                    if (v_nl[l][bkt].size() == 0) continue;
                    for (auto& pa_node : v_nl[l][bkt]) {
                        if (in_set(pa_node.id, xi) || !in_set(target_id, xi) || in_set(pa_node.suboptimal_extension, xi)) continue;
                        bfs_task<N>(sdat, target_id, pa_node, xi, l, v_nl);
                    } // for pa_node
                } // for bkt

            }, sp); // parallel_for
        } // for xi

        // quick memory release
        {
            auto tmp = tasks_table<N>(BUCKETS);
            std::swap(tmp, v_nl[l]);
        }


        visited += container_size(v_nl[l + 1]).first;
        if (l < n) Log.info() << "layer " << l << " processed, size of next layer: "
                              << container_size(v_nl[l + 1]).first << std::endl;
    } // for l

    auto rf = std::pow(2, n) / visited;

    Log.info() << "processed " << visited << " nodes" << std::endl;
    Log.info() << "reduction factor: " << std::fixed << rf << " (" << std::scientific << rf << ")"  << std::endl;

    return target_node;
} // bfs_core


template <int N>
std::pair<bool, std::string> search(const search_data<N>& sdat, bfs_node<N>& target_node) {
    using set_type = uint_type<N>;
    int n = sdat.mps_list.n();

    set_type X = set_full<set_type>(n);
    set_type E = set_empty<set_type>();

    bfs_node<N> source_node;

    source_node.score = 0;
    for (auto& x : source_node.path) { x = 0; }

    target_node = bfs_core(sdat, E, source_node, X);
    if (target_node.score == -1) { return {false, "unable to find any optimal structure"}; }

    return {true, ""};
} // search


template <int N>
std::pair<bool, std::string> read_search_write(int n, const SCC_config& scc_conf, const std::string& in, const std::string& out) {
    using set_type = uint_type<N>;

    search_data<N> sdat;
    sdat.scc_conf = scc_conf;

    Log.debug() << "processing with N = " << N << " " << sizeof(bit_util_base_type) << "B words" << std::endl;
    Log.info() << "reading MPS" << std::endl;

    auto res = sdat.mps_list.read(n, in);
    if (!res.first) return res;

    Log.info() << "MPS has " << sdat.mps_list.size() << " parent sets" << std::endl;
    Log.info() << "constructing opt table" << std::endl;

    sdat.opt.resize(n);

    for (int xi = 0; xi < n; ++xi) {
        auto opt = sdat.mps_list.optimal(xi);
        sdat.opt[xi] = {opt.pa, opt.s};
    }

    bfs_node<N> target_node;
    res = search(sdat, target_node);

    if (res.first == true) {
        Log.info() << "found optimal structure with score: " << std::fixed << target_node.score << std::endl;

        std::ofstream of(out);
        if (!of) return {false, "could not create output file"};
        for (int i = 0; i < n; ++i) of << static_cast<int>(target_node.path[i]) << " ";
        of.close();
    }

    return res;
} // read_search_write


int main(int argc, const char* argv[]) {
    InitLog();
    score_message("sabna-exsl-ssearch");

    int n = 0;

    int scc_limit = 6;
    float scc_density = 0.33;

    std::string mps_name;
    std::string ord_name;

    try {
        cxxopts::Options options(argv[0]);

        options.add_options()
            ("n,x-size", "number of variables", cxxopts::value<int>(n))
            ("scc-limit", "component size to process sequentially", cxxopts::value<int>(scc_limit))
            ("scc-density", "tau, SCC graph density to apply decomposition", cxxopts::value<float>(scc_density))
            ("mps-file", "input mps file", cxxopts::value<std::string>(mps_name))
            ("ord-file", "output order file", cxxopts::value<std::string>(ord_name))
            ("help", "print this help")
            ;

        auto opt_res = options.parse(argc, argv);

        if (!(opt_res.count("x-size") && opt_res.count("mps-file") && opt_res.count("ord-file")) || opt_res.count("help")) {
            std::cout << options.help() << std::endl;
            return 0;
        }

        if (n < 0) throw cxxopts::OptionException("Incorrect " + cxxopts::LQUOTE + "n" + cxxopts::RQUOTE + " option");
        if ((scc_density < 0.01) || (scc_density > 1.0)) throw cxxopts::OptionException("Incorrect " + cxxopts::LQUOTE + "scc-density" + cxxopts::RQUOTE + " option");

        if (!opt_res.unmatched().empty()) throw cxxopts::OptionException("Unrecognized options");
    } catch (const cxxopts::OptionException& e) {
        Log.error() << e.what() << std::endl;
        return -1;
    }

    Log.info() << "input: " << mps_name << std::endl;
    Log.info() << "variables: " << n << std::endl;
    Log.info() << "component limit: " << scc_limit << std::endl;
    Log.info() << "scc density: " << scc_density << std::endl;

    // work-around for mutex initialization
    {
        std::vector<std::vector<mutex_type>> tmp(n + 1);
        for (auto& x : tmp) x = std::vector<mutex_type>(BUCKETS);
        tmp.swap(gmtx_);
    }

    auto start = std::chrono::system_clock::now();

    SCC_config scc_conf{scc_limit, scc_density};

    std::pair<bool, std::string> res;

    if (n <= set_max_item<1>()) res = read_search_write<1>(n, scc_conf, mps_name, ord_name);
    else if (n <= set_max_item<2>()) res = read_search_write<2>(n, scc_conf, mps_name, ord_name);
    else if (n <= set_max_item<3>()) res = read_search_write<3>(n, scc_conf, mps_name, ord_name);
    else if (n <= set_max_item<4>()) res = read_search_write<4>(n, scc_conf, mps_name, ord_name);
    else {
        Log.error() << "unable to handle more than " << set_max_item<4>() << " variables!";
        return -1;
    }

    if (!res.first) {
        Log.error() << res.second << std::endl;
        return -1;
    }

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = end - start;
    Log.info() << "searching done in " << jaz::log::second_to_time(diff.count()) << std::endl;

    return 0;
} // main
