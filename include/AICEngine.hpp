/***
 *  $Id$
 **
 *  File: AICEngine.hpp
 *  Created: Aug 18, 2017
 *
 *  Author: Subhadeep Karan <skaran@buffalo.edu>
 *  Copyright (c) 2017 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef AIC_ENGINE_HPP
#define AIC_ENGINE_HPP

#include "AIC.hpp"
#include "ScoringEngine.hpp"
#include "log.hpp"


template <int N, typename CountingQueryEngine>
class AICEngine : public ScoringEngine<N> {
public:
    using set_type = uint_type<N>;
    using score_type = AIC::score_type;

    AICEngine(CountingQueryEngine& cqe, AIC& aic, int ps = -1) : ScoringEngine<N>(ps), cqe_(cqe), aic_(aic) {
        n_ = cqe_.n();
        m_ = cqe_.m();

        E_ = set_empty<set_type>();
        X_ = set_full<set_type>(n_);

        H_.resize(n_, -1);
        md_.resize(n_ + 1, E_);
        md_[0] = X_;
        norder_.resize(n_, -1);

        m_init_entropy__();
        //if (cqe_.is_reorderable()) { m_reorder_counting_query_engine__(); }
        m_max_pa_size__();
    } // AICEngine

    int n() const { return n_; }

    int m() const { return m_; }

    bool process(const set_type& pa, set_type& ch, MPSList<N>& mps_list, std::vector<std::pair<int, typename MPS<N>::MPSNode>>& mps_buffer, int l) {
        std::vector<AIC> vec_aic(set_size(ch), aic_);
        set_type ch_copy = ch;

        cqe_.apply(ch, pa, vec_aic);

        for (int xi = 0, idx = 0; xi < n_; ++xi) {
            if (!in_set(ch, xi)) continue;
            if (!m_extend_insert__(xi, pa, vec_aic[idx].score(), mps_list, mps_buffer)) { ch = set_remove(ch, xi); }
            ++idx;
        }

        // setting flag - true iff the xi is pruned due to cond 1 | 2
        bool flag = true;
        if (ch == ch_copy) { flag = false; }
        ch = ch & md_[l + 1];

        return flag;
    } // process

    void finalize(MPSList<N>& mps_list) {
        if (is_reordered) { mps_list.map_variables(norder_); }
    } // finalize


private:
    void m_init_entropy__() {
        std::vector<AIC> vec_aic(1, aic_);
        for (int xi = 0; xi < n_; ++xi) {
            cqe_.apply(set_add(E_, xi), set_remove(X_, xi), vec_aic);
            auto res = vec_aic[0].score();
            H_[xi] = res.second;
        }
    } // m_init_entropy__

    bool m_extend_insert__(int xi, const set_type& pa, const AIC::score_type& score, MPSList<N>& mps_list, std::vector<std::pair<int, typename MPS<N>::MPSNode>>& mps_buffer) {
        auto res = mps_list.find(xi, pa);
        if (score.first < res.s) { mps_buffer.push_back({xi, {score.first, pa} }); }
        else if (score.first - score.second + H_[xi] >= res.s) { return false; }
        return true;
    } // m_extend_and_insert__

    void m_reorder_counting_query_engine__() {
        struct PNode {
            int xi;
            int ri;
            double H;

            bool operator<(const PNode& rhs) const {
                if (ri != rhs.ri) return ri > rhs.ri;
                return H < rhs.H;
            }
        }; // struct PNode

        std::vector<PNode> v(n_);
        std::vector<AIC> vec_aic;
        vec_aic.resize(1, aic_);

        for (int xi = 0; xi < n_; ++xi) {
            cqe_.apply(set_add(E_, xi), set_remove(X_, xi), vec_aic);
            auto res = vec_aic[0].score();
            v[xi] = PNode{xi, cqe_.r(xi), res.second};
        }

        std::sort(std::begin(v), std::end(v));

        for (int i = 0; i < n_; ++i) {
            norder_[i] = v[i].xi;
            H_[i] = v[i].H;
        }

        is_reordered = cqe_.reorder(norder_);
    } // m_reorder_counting_query_engine__

    void m_max_pa_size__() {
        std::vector<AIC> vec_aic(n_, aic_);
        cqe_.apply(X_, E_, vec_aic);

        std::vector<std::pair<int, int>> vec_xi_r(n_, {0, 0});
        for (int xi = 0; xi < n_; ++xi) { vec_xi_r[xi] = {xi, cqe_.r(xi)}; }

        std::sort(vec_xi_r.begin(), vec_xi_r.end(),
            [] (const std::pair<int, int>& lhs, const std::pair<int, int>& rhs) {
                if (lhs.second == rhs.second) { return lhs.first < rhs.first;  }
                return lhs.second < rhs.second;
                }
        );

        if ((this->pa_size < 1) || (this->pa_size > n_ - 1)) this->pa_size = n_;
        Log.info() << "maximal number of parents limited to " << this->pa_size << std::endl;

        for (int xi = 0; xi < n_; ++xi) {
            double thres = vec_aic[xi].score().first - H_[xi];
            double val = vec_aic[xi].score().first - vec_aic[xi].score().second;

            for (int j = 0, l = 0; (j < n_) && (l < this->pa_size); ++j) {
                int xj = vec_xi_r[j].first;
                int r_xj = vec_xi_r[j].second;
                if (xi != xj) {
                    val *= r_xj;
                    if (val >= thres) break;
                    ++l;
                    md_[l] = set_add(md_[l], xi);
                }
            } // for j
        } // for xi

     } // m_max_pa_size__

    int n_ = -1;
    int m_ = -1;

    CountingQueryEngine& cqe_;

    set_type X_;
    set_type E_;

    bool is_reordered = false;

    std::vector<double> H_;
    std::vector<int> norder_;
    std::vector<set_type> md_;

    AIC aic_;

}; // class AICEngine

#endif // AIC_ENGINE_HPP
