/***
 *  $Id$
 **
 *  File: eval.hpp
 *  Created: Sep 12, 2018
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2018 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 */

#ifndef EVAL_HPP
#define EVAL_HPP

#include <unordered_map>
#include <vector>

#include "bit_util.hpp"


template <int N> class Eval {
public:
    explicit Eval(int n) : cache_(n) { }

    template <typename ScoreFunction, typename CQE>
    double operator()(const std::vector<uint_type<N>>& net, CQE& cqe, ScoreFunction F) {
        double S = 0.0;
        double s = 0.0;

        for (int xi = 0; xi < net.size(); ++xi) {
            auto pos = cache_[xi].find(net[xi]);
            if (pos != cache_[xi].end()) s = pos->second;
            else {
                cqe.apply(xi, net[xi], F);
                s = std::get<0>(F.score());
                cache_[xi].insert({net[xi], s});
            }
            S += s;
        }

        return S;
    } // operator()

private:
    std::vector<std::unordered_map<uint_type<N>, double, uint_hash>> cache_;

}; // class Eval

#endif // EVAL_HPP
