/***
 *  $Id$
 **
 *  File: math.hpp
 *  Developed: Mar 11, 2005
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2004-2016 Jaroslaw Zola
 *  Distributed under the Boost Software License, Version 1.0.
 *  See accompanying file LICENSE_BOOST.txt.
 *
 *  This file is part of jaz.
 */

#ifndef JAZ_MATH_HPP
#define JAZ_MATH_HPP

#include <vector>


namespace jaz {

  /** Function: nc2
   *  Computes "n choose 2".
   *
   *  Parameters:
   *  n - must be an integer.
   */
  template <typename Int> constexpr Int nc2(Int n) { return (n * (n - 1)) >> 1; }


  /** Function: ipow
   *  Computes an integer power of x using exponentiation by squaring.
   *  If y is not a positive integer behavior is undefined.
   *
   *  Parameters:
   *  x - Base.
   *  y - Exponent (must be a positive integer).
   */
  template <typename T, typename UInt> T ipow(T x, UInt y) {
      T res = 1;

      while (y) {
          if (y & 1) res *= x;
          y >>= 1;
          x *= x;
      }

      return res;
  } // ipow


  /** Function: fac
   *  Computes factorial of x using a simple recursive expression.
   *
   *  To get correct result x must be less than 20 (20! is the max value
   *  that can be stored on 64 bits.
   */
  constexpr unsigned long long int fac(unsigned char x) {
      return (x < 2) ? 1 : x * fac(x - 1);
  } // fac


  /** Class: Binom
   *  Functor to compute binomial B(n,k).
   *  Uses O(n) storage.
   */
  template <typename UInt> class Binom {
  public:
      typedef UInt result_type;
      typedef UInt first_argument_type;
      typedef UInt second_argument_type;

      /** Function: operator()
       *
       *  Parameters:
       *  n - must be a positive integer.
       *  k - must be a positive integer less than n.
       */
      result_type operator()(first_argument_type n, second_argument_type k) {
          if (n_ != n) {
              n_ = n;

              n = n + 1;
              B_.resize(n);

              B_[0] = 1;
              for (auto i = 1; i < n; ++i) {
                  B_[i] = 1;
                  for (auto j = i - 1; j > 0; --j) B_[j] += B_[j - 1];
              }
          } // if n

          return B_[k];
      } // operator()

  private:
      int n_ = -1;
      std::vector<result_type> B_;

  }; // class Binom


  /** Class: Comb
   *  Generator to enumerate all possible B(n,k) combinations
   *  of the set [0,n). Uses O(k) storage.
   */
  template <typename UInt = int> class Comb {
  public:
      typedef UInt value_type;

      explicit Comb(int n = 0, int k = 0) { reset(n, k); }

      void reset(int n, int k) {
          n_ = n;
          k_ = k;
          k1_ = k - 1;
          nk_ = n - k;
          data_.resize(k_);
          for (int i = 0; i < k_; ++i) data_[i] = i;
      } // reset

      bool next() {
          int i = k1_;
          while ((i > 0) && (data_[i] == nk_ + i)) --i;
          if ((i == 0) && (data_[i] == nk_)) return false;
          ++data_[i];
          for (; i < k1_; ++i) data_[i + 1] = data_[i] + 1;
          return true;
      } // next

      const std::vector<value_type>& data() const { return data_; }

      value_type operator[](int i) const { return data_[i]; }


  private:
      int n_;
      int k_;
      int k1_;
      int nk_;
      std::vector<value_type> data_;

  }; // class Comb


  /** Class CombIndex
   *  Functor to compute lexicographic index of a k-element combination
   *  from the set [0, n). Based on the kSubsetLexRank algorithm.
   *  Uses O(n^2) storage.
   */
  template <typename UInt = int>
  class CombIndex {
  public:
      typedef UInt result_type;

      template <typename Sequence>
      result_type operator()(int n, const Sequence& seq) {
          int k = seq.size();
          result_type r = 0;

          if (n_ < n) {
              C_.resize(n);
              n_ = n;
          }

          // case for i == 0 to avoid if
          for (int j = 0; j < seq[0]; ++j) r += C_[n - j - 1](n - j - 1, k - 1);

          for (int i = 1; i < k; ++i) {
              for (int j = seq[i - 1] + 1; j < seq[i]; ++j) r += C_[n - j - 1](n - j - 1, k - i - 1);
          }

          return r;
      } // operaror()

  private:
      int n_ = 0;
      std::vector<Binom<result_type>> C_;

  }; // class CombIndex

} // namespace jaz

#endif // JAZ_MATH_HPP
