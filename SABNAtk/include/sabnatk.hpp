/***
 *  $Id$
 **
 *  File: sabnatk.hpp
 *  Created: May 20, 2018
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *  Copyright (c) 2016-2018 SCoRe Group http://www.score-group.org/
 *  Distributed under the MIT License.
 */

#ifndef SABNATK_HPP
#define SABNATK_HPP

#include "BVCounter.hpp"
#include "CTCounter.hpp"
#include "RadCounter.hpp"

namespace sabnatk {

  enum counter_type { BV, CT, Rad };

  template <int N, counter_type Type, typename Data = uint8_t, typename Iter>
  auto create_counter(int n, int m, Iter iter) {
      if constexpr (Type == BV) return create_BVCounter<N>(n, m, iter);
      if constexpr (Type == CT) return create_CTCounter<N>(n, m, iter);
      if constexpr (Type == Rad) return create_RadCounter<N, Data>(n, m, iter);
  } // create_counter

} // namespace sabnatk

#endif // SABNATK_HPP
