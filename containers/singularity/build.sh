#!/bin/bash

VERSION=`grep \#define ../../src/config.hpp | grep VERSION | awk '{print $3}' | sed -e 's/\"//g'`
SUFFIX=""

if [ `id -u` -ne 0 ]; then
  SUDO="sudo"
fi

$SUDO singularity build sabna-$VERSION$SUFFIX.simg sabna.def
