#!/bin/bash

DIR=$(dirname "$0")

NAME=$(basename -- "$1")
NAME="${NAME%.*}"

echo "$NAME"

rm -rf $NAME
$DIR/tools/csv-prepare.py "$1" $NAME

SCORE="mdl"

PRIORS=""
EXT=$SCORE
if [ -n "$2" ]; then
  PRIORS="$NAME/$NAME.sabna.priors"
  $DIR/tools/priors-prepare.py "$2" $NAME/$NAME.sabna.variables $PRIORS
  PRIORS="--priors-file $PRIORS"
  EXT="$SCORE+"
fi

N=`cat $NAME/$NAME.sabna.variables | wc -l`

$DIR/bin/sabna-exsl-mpsbuild --csv-file $NAME/$NAME.sabna.csv --mps-file $NAME.sabna.$EXT.mps --score $SCORE $PRIORS
$DIR/bin/sabna-exsl-ssearch -n $N --mps-file $NAME.sabna.$EXT.mps --ord-file $NAME.sabna.$EXT.ord
$DIR/bin/sabna-order2net --csv-file $NAME/$NAME.sabna.csv --mps-file $NAME.sabna.$EXT.mps --ord-file $NAME.sabna.$EXT.ord --net-name $NAME.sabna.$EXT --format sif
$DIR/bin/sabna-pl-mle --csv-file $NAME/$NAME.sabna.csv --sif-file $NAME.sabna.$EXT.sif --bif-file $NAME.sabna.$EXT.bif
$DIR/tools/bif-format.py --map-variables $NAME/$NAME.sabna.variables --map-states $NAME/$NAME.sabna.states $NAME.sabna.$EXT.bif $NAME.$EXT.bif $NAME
$DIR/tools/sif-format.py --map-variables $NAME/$NAME.sabna.variables $NAME.sabna.$EXT.sif $NAME.$EXT.sif
